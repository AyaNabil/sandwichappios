//
//  CardMethodViewController.swift
//  SandwishApp
//
//  Created by mac on 3/10/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class CardMethodViewController: UIViewController {
    
    @IBOutlet weak var cardnameTF: UITextField!
    @IBOutlet weak var cardnaumberField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //shadow for cardname
        cardnameTF.layer.masksToBounds = false
        cardnameTF.layer.shadowRadius = 3.0
        cardnameTF.layer.shadowColor = UIColor.gray.cgColor
        cardnameTF.layer.shadowOffset =  CGSize(width: 1, height: 4)
        cardnameTF.layer.shadowOpacity = 0.6
        //shadow for cardnumber
        cardnaumberField.layer.masksToBounds = false
        cardnaumberField.layer.shadowRadius = 3.0
        cardnaumberField.layer.shadowColor = UIColor.gray.cgColor
        cardnaumberField.layer.shadowOffset =  CGSize(width: 1, height: 4)
        cardnaumberField.layer.shadowOpacity = 0.6

   
    }
    @IBAction func checkoutButton(_ sender: UIButton) {
                let storyboard =  UIStoryboard(name: "Order", bundle: nil)
                let userLogin = storyboard.instantiateViewController(withIdentifier: "OrderOnwayViewController") as! OrderOnwayViewController
                self.navigationController?.pushViewController(userLogin, animated: true)
        
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    


}
