//
//  OrdersViewController.swift
//  SandwishApp
//
//  Created by mac on 3/8/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class OrdersViewController: UIViewController , UITableViewDataSource , UITableViewDelegate {
    
       @IBOutlet var orderTableView: UITableView!
    var imageNames : [String] = []
    var labelNames : [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        labelNames = ["Fried Chicken Box" , "Fried Chicken Box" ,"Fried Chicken Box" , "Fried Chicken Box", "Fried Chicken Box"]
        
        imageNames = ["redKfc" , "redKfc" , "redKfc" , "redKfc" , "redKfc"]
    }
    
    
    
    //tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelNames.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = orderTableView.dequeueReusableCell(withIdentifier: "OrdersTableViewCell", for: indexPath) as!
        OrdersTableViewCell
        cell.ordertitle.text = labelNames[indexPath.row]
        cell.orderPicture.image = UIImage(named:imageNames[indexPath.row])
        //        let backgroundView = UIView()
        //        //        backgroundView.backgroundColor = UIColor.orange
        //        backgroundView.backgroundColor = UIColor(red: 212/225, green: 156/255, blue: 241/255, alpha: 1)
        //        backgroundView.layer.cornerRadius = 25
        //        cell.selectedBackgroundView = backgroundView
        //
        //        cell.separatorInset = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
        
        
        
        return cell
    }
    
    @IBAction func backButton(_ sender: UIButton) {
//        self.navigationController?.popViewController(animated: true)
        let storyboard =  UIStoryboard(name: "Home", bundle: nil)
        let userLogin = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(userLogin, animated: true)
        
    }
    
    
    
    @IBAction func checkoutbutton(_ sender: UIButton) {
        
        let storyboard =  UIStoryboard(name: "Checkout", bundle: nil)
        let userLogin = storyboard.instantiateViewController(withIdentifier: "CheckoutViewController") as! CheckoutViewController
        self.navigationController?.pushViewController(userLogin, animated: true)
        
        
    }

}
