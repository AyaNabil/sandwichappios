//
//  SecondPageViewController.swift
//  SandwishApp
//
//  Created by mac on 3/3/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class SecondPageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func nextButtonAction(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Start", bundle:nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ThirdPageViewController") as! ThirdPageViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
    }
    
    
    @IBAction func skipButtonAction(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Start", bundle:nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "FirstPageViewController") as! FirstPageViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }

}
