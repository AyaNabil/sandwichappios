//
//  LoginViewController.swift
//  SandwishApp
//
//  Created by mac on 3/4/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import FacebookShare
import GoogleSignIn

class LoginViewController: UIViewController  , GIDSignInDelegate , GIDSignInUIDelegate{
   
    
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var btnGoogle: UIButton!
     var email: String!
     var password: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        //btngoogle
        btnGoogle.addTarget(self, action: #selector(loginByGmail(_ :)), for: .touchUpInside)
        
        email = "ayanabil194314@gmail.com"
        password = "000000"
        #if DEBUG
        emailTextField.text = "ayanabil194314@gmail.com"
        passwordTF.text = "000000"
        #endif
    
        //password
        passwordTF.isSecureTextEntry = true
        passwordTF.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: passwordTF.frame.height))
        passwordTF.leftViewMode = .always
        //email
        emailTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: emailTextField.frame.height))
        emailTextField.leftViewMode = .always
        
        //shadow for email
        emailTextField.layer.masksToBounds = false
        emailTextField.layer.shadowRadius = 3.0
        emailTextField.layer.shadowColor = UIColor.gray.cgColor
        emailTextField.layer.shadowOffset =  CGSize(width: 1, height: 4)
        emailTextField.layer.shadowOpacity = 0.6
        //shadow for password
        passwordTF.layer.masksToBounds = false
        passwordTF.layer.shadowRadius = 3.0
        passwordTF.layer.shadowColor = UIColor.gray.cgColor
        passwordTF.layer.shadowOffset =  CGSize(width: 1, height: 4)
        passwordTF.layer.shadowOpacity = 0.6
        
}
    
    
    @IBAction func passwordClicked(_ sender: Any) {
        
        (sender as! UIButton).isSelected = !(sender as! UIButton).isSelected
        if (sender as! UIButton).isSelected {
            passwordTF.isSecureTextEntry = true
        } else {
            passwordTF.isSecureTextEntry = false
        }
    }
    
    
    @IBAction func forgetPassword(_ sender: UIButton) {
        
        let storyboard =  UIStoryboard(name: "Login", bundle: nil)
        let forgetpass = storyboard.instantiateViewController(withIdentifier: "ForgetPasswordViewController") as! ForgetPasswordViewController
        self.navigationController?.pushViewController(forgetpass, animated: true)
        //        self.present(forgetpass, animated: true)
        
    }
    
  
    @IBAction func createAccount(_ sender: UIButton) {
        
        let storyboard =  UIStoryboard(name: "Login", bundle: nil)
        let userLogin = storyboard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(userLogin, animated: true)
//        //           self.present(userLogin, animated: true)
        
    }
    
    
    @IBAction func loginByFacebook(_ sender: UIButton) {
        
        let manager = LoginManager()
        manager.logIn(permissions: [.publicProfile , .email], viewController: self) { (result) in
            switch result {
                
                
            case .cancelled:
                print("user cancelled login process")
                

                break
          
            case .failed(let error):
                print("login process failed with error \(error.localizedDescription)")
                break
                
            case .success(let granted, let declined, let accessToken):
                print("access token == \(accessToken)")
                print("login successfully")
                let alertController = UIAlertController(title: "Welcome", message: "login successfully", preferredStyle: UIAlertController.Style.alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                print("ok")
//                    let storyboard =  UIStoryboard(name: "Login", bundle: nil)
//                    let forgetpass = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//                    self.navigationController?.pushViewController(forgetpass, animated: true)
                }))
//
                
                self.present(alertController, animated: true, completion: nil)
                
            }
        }
      
        
    }
    //sign in by google
    
    @IBAction func loginByGmail(_ sender: UIButton) {
        if btnGoogle.title(for: .normal) == "SignOut"
        {
            GIDSignIn.sharedInstance()?.signOut()
        }
        else
        {
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.uiDelegate = self
        GIDSignIn.sharedInstance()?.signIn()
//            print("login successfully")
//            let alertController = UIAlertController(title: "Welcome", message: "login successfully", preferredStyle: UIAlertController.Style.alert)
//            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
//                print("ok")
//                //                    let storyboard =  UIStoryboard(name: "Login", bundle: nil)
//                //                    let forgetpass = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//                //                    self.navigationController?.pushViewController(forgetpass, animated: true)
//            }))
//            //
//
//            self.present(alertController, animated: true, completion: nil)
        }
      
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            
            print("we have ann error \(error.localizedDescription)")
        }
        else{
            if let gmailUser = user{
                btnGoogle.setTitle("SignOut", for: .normal)
            }
            
        }
    }
    
    //instgram button
    @IBAction func instgramButton(_ sender: Any) {
        
    }
    
    @IBAction func loginButton(_ sender: Any) {
        if emailTextField.text == email && passwordTF.text == password
        {
            
            let alertController = UIAlertController(title: "Welcome", message: "login successfully", preferredStyle: UIAlertController.Style.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                
                let storyboard =  UIStoryboard(name: "Home", bundle: nil)
                let forgetpass = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                self.navigationController?.pushViewController(forgetpass, animated: true)
            }))
            
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        else if emailTextField.text == "" && passwordTF.text == "" {
            return
            
        }
        else{
            let alertController = UIAlertController(title: "Wrong", message: "Wrong username or password ", preferredStyle: UIAlertController.Style.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        
        
    }
    
    
    


}
