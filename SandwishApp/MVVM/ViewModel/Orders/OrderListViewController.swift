//
//  OrderListViewController.swift
//  SandwishApp
//
//  Created by mac on 3/8/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class OrderListViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource , UITableViewDelegate , UITableViewDataSource , UICollectionViewDelegateFlowLayout {
    var imageNames : [String] = []
    var labelNames : [String] = []
    
    var imageNames1 : [String] = []
    var labelNames1 : [String] = []
 
     @IBOutlet var orderTableView: UITableView!
     @IBOutlet var textfield: UITextField!
    @IBOutlet var searchtextfield: UITextField!
    
     @IBOutlet var orderCollectionview: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        orderCollectionview.allowsMultipleSelection = true
        //textfield
        textfield.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: textfield.frame.height))
        textfield.leftViewMode = .always
        
        //searchtextfield
        searchtextfield.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: searchtextfield.frame.height))
        searchtextfield.leftViewMode = .always

        orderTableView.delegate = self
        orderTableView.dataSource = self
        
        orderCollectionview.delegate = self
        orderCollectionview.dataSource = self
        
        //shadow for textfield
        textfield.layer.masksToBounds = false
        textfield.layer.shadowRadius = 3.0
        textfield.layer.shadowColor = UIColor.gray.cgColor
        textfield.layer.shadowOffset =  CGSize(width: 1, height: 4)
        textfield.layer.shadowOpacity = 0.6
        
        
        labelNames = ["Near Me" , "Cuisines","Fast Delivery" , "Rating" , "Offers"]
        
        imageNames = ["markerpurple" , "purplehat" , "mandelivery" , "Review" , "Best Price"]
        
        labelNames1 = ["Fast food,Sandwiches" , "Fast food,Sandwiches" , "Fast food,Sandwiches" , "Fast food,Sandwiches" , "Fast food,Sandwiches"]
        
        imageNames1 = ["logokfc" , "logokfc" , "logokfc" , "logokfc" , "logokfc"]
        
    }
    
    //collectionview
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           return labelNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
      let cell = orderCollectionview.dequeueReusableCell(withReuseIdentifier: "orderCollectionViewCell", for: indexPath)
            as! orderCollectionViewCell
        cell.ordertitle.text = labelNames[indexPath.row]
        cell.orderPicture.image = UIImage(named:imageNames[indexPath.row])
        //cell color
         let items = orderCollectionview.indexPathsForSelectedItems
        print(items!)
              let backgroundView = UIView()
              backgroundView.backgroundColor = UIColor.orange
              backgroundView.backgroundColor = UIColor(red: 212/225, green: 156/255, blue: 241/255, alpha: 1)
        //        backgroundView.layer.cornerRadius = 25
               cell.selectedBackgroundView = backgroundView
        //
        //        cell.separatorInset = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
        let collectionColor = [UIColor(displayP3Red: 248/255, green: 218/255, blue: 120/255, alpha: 1) ,
                               
                               UIColor(displayP3Red: 221/255, green: 194/255, blue: 105/255, alpha: 1) ,
                               UIColor(displayP3Red: 240/255, green: 198/255, blue: 61/255, alpha: 1)]
        
        
        if indexPath.row % 3 == 0{
            cell.backgroundColor = collectionColor[0]
        }
            
        else  if indexPath.row % 3 == 1{
            cell.backgroundColor = collectionColor[1]
        }
            
        else{
            cell.backgroundColor = collectionColor[2]
        }
        
        return cell
    }
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
//collectionview didselect
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.row  {
        case 0:
             print("hi")
         
            
        
        case 1:
            
            
            
            let sb = UIStoryboard(name: "Review", bundle:nil)
            let vc = sb.instantiateViewController(withIdentifier: "CuisinesViewController") as! CuisinesViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        case 2:
            
            
               print("hi")
            
        case 3:
            
         print("hi")
            
        case 4:
               print("hi")
            
            break
        default:
            
            
            break
        }
    }
    
    
    //tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return labelNames1.count
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = orderTableView.dequeueReusableCell(withIdentifier: "OrderTableViewCell", for: indexPath) as!
        OrderTableViewCell
        cell.ordertitle.text = labelNames1[indexPath.row]
        cell.orderPicture.image = UIImage(named:imageNames1[indexPath.row])
//        let backgroundView = UIView()
//        //        backgroundView.backgroundColor = UIColor.orange
//        backgroundView.backgroundColor = UIColor(red: 212/225, green: 156/255, blue: 241/255, alpha: 1)
//        backgroundView.layer.cornerRadius = 25
//        cell.selectedBackgroundView = backgroundView
//
//        cell.separatorInset = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
        

        return cell
    }
    
    
    //didselect
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        orderTableView.deselectRow(at: indexPath, animated: true)
        let cell = orderTableView.cellForRow(at: indexPath) as! OrderTableViewCell
        let storyboard =  UIStoryboard(name: "Order", bundle: nil)
        let userLogin = storyboard.instantiateViewController(withIdentifier: "MenuListViewController") as! MenuListViewController
        self.navigationController?.pushViewController(userLogin, animated: true)
    }
    
    @IBAction func backButton(_ sender: UIButton) {
//        self.navigationController?.popViewController(animated: true)
        let storyboard =  UIStoryboard(name: "Home", bundle: nil)
        let userLogin = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(userLogin, animated: true)
    }
    

}
