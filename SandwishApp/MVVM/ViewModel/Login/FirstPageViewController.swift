//
//  FirstPageViewController.swift
//  SandwishApp
//
//  Created by mac on 3/3/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class FirstPageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func nextButtonAction(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Start", bundle:nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "SecondPageViewController") as! SecondPageViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
    }

//
    @IBAction func skipButtonAction(_ sender: UIButton) {
        
       
        
        let storyboard = UIStoryboard(name: "Login", bundle:nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }


}
