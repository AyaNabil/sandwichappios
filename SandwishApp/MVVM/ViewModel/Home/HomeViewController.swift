//
//  HomeViewController.swift
//  SandwishApp
//
//  Created by mac on 3/5/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func startOrderingbutton(_ sender: UIButton) {
        
        let storyboard =  UIStoryboard(name: "Order", bundle: nil)
        let userLogin = storyboard.instantiateViewController(withIdentifier: "NoOrderViewController") as! NoOrderViewController
        self.navigationController?.pushViewController(userLogin, animated: true)
        
        
    }
    
    

}
