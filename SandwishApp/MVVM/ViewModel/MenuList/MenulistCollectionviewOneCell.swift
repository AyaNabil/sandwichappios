//
//  MenulistCollectionviewOneCell.swift
//  SandwishApp
//
//  Created by mac on 3/9/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class MenulistCollectionviewOneCell: UICollectionViewCell {
    
    @IBOutlet weak var menutitle: UILabel!
    @IBOutlet weak var menuPicture: UIImageView!
    @IBOutlet weak var view: UIView!
}
