//
//  AccountSettingViewController.swift
//  SandwishApp
//
//  Created by mac on 3/5/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class AccountSettingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }

    
    @IBAction func editaccounttbutton(_ sender: UIButton) {
        
        let storyboard =  UIStoryboard(name: "Home", bundle: nil)
        let userLogin = storyboard.instantiateViewController(withIdentifier: "EditAccountViewController") as! EditAccountViewController
        self.navigationController?.pushViewController(userLogin, animated: true)
     
        
    }

}
