//
//  CuisinesCollectionViewCell.swift
//  SandwishApp
//
//  Created by mac on 3/9/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class CuisinesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cuisinestitle: UILabel!
    
    override var isSelected: Bool {
        didSet {
            if self.isSelected {
                backgroundColor = UIColor(red: 252/225, green: 194/255, blue: 7/255, alpha: 1)
            }
            else {
                backgroundColor = UIColor.white
            }
        }
    }
}

