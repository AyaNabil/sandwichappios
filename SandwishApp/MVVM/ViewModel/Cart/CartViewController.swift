//
//  CartViewController.swift
//  SandwishApp
//
//  Created by mac on 3/10/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {

    var someValue: Int = 1 {
        didSet {
            countlabel.text = "\(someValue)"
        }
    }
    @IBOutlet var countlabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func addToCartButton(_ sender: UIButton) {
        let storyboard =  UIStoryboard(name: "Order", bundle: nil)
        let userLogin = storyboard.instantiateViewController(withIdentifier: "OrdersViewController") as! OrdersViewController
        self.navigationController?.pushViewController(userLogin, animated: true)
        
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    var unchecked = true
    
    @IBAction func smallButton(sender: UIButton) {
        if unchecked {
            sender.setImage(UIImage(named:"checked"), for: UIControl.State.normal)
            unchecked = false
        }
        else {
             sender.setImage(UIImage(named:"unchecked"), for: UIControl.State.normal)
            unchecked = true
        }
    }
    
    @IBAction func mediumButton(sender: UIButton) {
        if unchecked {
            sender.setImage(UIImage(named:"checked"), for: UIControl.State.normal)
            unchecked = false
        }
        else {
            sender.setImage(UIImage(named:"unchecked"), for: UIControl.State.normal)
            unchecked = true
        }
    }

    
    @IBAction func largeButton(sender: UIButton) {
        if unchecked {
            sender.setImage(UIImage(named:"checked"), for: UIControl.State.normal)
            unchecked = false
        }
        else {
            sender.setImage(UIImage(named:"unchecked"), for: UIControl.State.normal)
            unchecked = true
        }
    }
    
    @IBAction func regularButton(sender: UIButton) {
        if unchecked {
            sender.setImage(UIImage(named:"checked"), for: UIControl.State.normal)
            unchecked = false
        }
        else {
            sender.setImage(UIImage(named:"unchecked"), for: UIControl.State.normal)
            unchecked = true
        }
    }
    
    
    @IBAction func spicyButton(sender: UIButton) {
        if unchecked {
            sender.setImage(UIImage(named:"checked"), for: UIControl.State.normal)
            unchecked = false
        }
        else {
            sender.setImage(UIImage(named:"unchecked"), for: UIControl.State.normal)
            unchecked = true
        }
    }
    
    
    
    @IBAction func oneButton(sender: UIButton) {
        if unchecked {
            sender.setImage(UIImage(named:"checked"), for: UIControl.State.normal)
            unchecked = false
        }
        else {
            sender.setImage(UIImage(named:"unchecked"), for: UIControl.State.normal)
            unchecked = true
        }
    }
    
    
    
    @IBAction func twoButton(sender: UIButton) {
        if unchecked {
            sender.setImage(UIImage(named:"checked"), for: UIControl.State.normal)
            unchecked = false
        }
        else {
            sender.setImage(UIImage(named:"unchecked"), for: UIControl.State.normal)
            unchecked = true
        }
    }

    
    @IBAction func threeButton(sender: UIButton) {
        if unchecked {
            sender.setImage(UIImage(named:"checked"), for: UIControl.State.normal)
            unchecked = false
        }
        else {
            sender.setImage(UIImage(named:"unchecked"), for: UIControl.State.normal)
            unchecked = true
        }
    }
    
    
    @IBAction func fourButton(sender: UIButton) {
        if unchecked {
            sender.setImage(UIImage(named:"checked"), for: UIControl.State.normal)
            unchecked = false
        }
        else {
            sender.setImage(UIImage(named:"unchecked"), for: UIControl.State.normal)
            unchecked = true
        }
    }
    
    
    
    @IBAction func fiveButton(sender: UIButton) {
        if unchecked {
            sender.setImage(UIImage(named:"checked"), for: UIControl.State.normal)
            unchecked = false
        }
        else {
            sender.setImage(UIImage(named:"unchecked"), for: UIControl.State.normal)
            unchecked = true
        }
    }
    
    
    
    @IBAction func sixButton(sender: UIButton) {
        if unchecked {
            sender.setImage(UIImage(named:"checked"), for: UIControl.State.normal)
            unchecked = false
        }
        else {
            sender.setImage(UIImage(named:"unchecked"), for: UIControl.State.normal)
            unchecked = true
        }
    }
    
    
    @IBAction func sevenButton(sender: UIButton) {
        if unchecked {
            sender.setImage(UIImage(named:"checked"), for: UIControl.State.normal)
            unchecked = false
        }
        else {
            sender.setImage(UIImage(named:"unchecked"), for: UIControl.State.normal)
            unchecked = true
        }
    }
    
    
    @IBAction func eightButton(sender: UIButton) {
        if unchecked {
            sender.setImage(UIImage(named:"checked"), for: UIControl.State.normal)
            unchecked = false
        }
        else {
            sender.setImage(UIImage(named:"unchecked"), for: UIControl.State.normal)
            unchecked = true
        }
    }
    
    @IBAction func plusButton(sender: UIButton) {

          someValue += 1
        
    }
    
    @IBAction func minusButton(sender: UIButton) {
          someValue -= 1
    }
}
