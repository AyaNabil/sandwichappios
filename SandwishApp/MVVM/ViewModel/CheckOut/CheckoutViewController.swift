//
//  CheckoutViewController.swift
//  SandwishApp
//
//  Created by mac on 3/10/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class CheckoutViewController:UIViewController,UITextFieldDelegate , UIPickerViewDataSource , UIPickerViewDelegate{
  
    
    @IBOutlet var textfield: UITextField!
     @IBOutlet weak var picker: UIPickerView!
    var labelNames : [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        labelNames = ["23 Saint Peter , Maadi" , "26 Saint Peter , Nasr Street ","24 Saint Peter , Elshrouk" , "27 Saint Peter , ElRehab" , "25 Saint Peter , Nasr Street" ]
        // Connect data:
        self.picker.delegate = self
        self.picker.dataSource = self
        
       
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
         return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
         return labelNames.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
          return labelNames[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        textfield.text = labelNames[row]
//        picker.isHidden = true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        picker.isHidden = true
        return false
    }
    
    
    @IBAction func continuButton(_ sender: UIButton) {
                let storyboard =  UIStoryboard(name: "Checkout", bundle: nil)
                let userLogin = storyboard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                self.navigationController?.pushViewController(userLogin, animated: true)
        
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }


}
