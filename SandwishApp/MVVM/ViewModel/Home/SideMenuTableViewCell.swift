//
//  SideMenuTableViewCell.swift
//  SandwishApp
//
//  Created by mac on 3/5/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {
    @IBOutlet weak var sideMenutitle: UILabel!
    @IBOutlet weak var sideMenuPicture: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
