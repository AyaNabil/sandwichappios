//
//  LanguageViewController.swift
//  SandwishApp
//
//  Created by mac on 3/9/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class LanguageViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {

    
    @IBOutlet var languageTableView: UITableView!
    
    var imageNames : [String] = []
    var labelNames : [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        languageTableView.delegate = self
        languageTableView.dataSource = self
        labelNames = ["Arabic" , "English ","French" , "German" , "Turkish" ]
        
        imageNames = ["egypt" , "united-kingdom" , "france" , "germany" ,"turkey"]
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = languageTableView.dequeueReusableCell(withIdentifier: "LanguageTableViewCell", for: indexPath) as!
        LanguageTableViewCell
        cell.languagetitle.text = labelNames[indexPath.row]
        cell.languagePicture.image = UIImage(named:imageNames[indexPath.row])
        let backgroundView = UIView()
                backgroundView.backgroundColor = UIColor.lightGray
//        backgroundView.backgroundColor = UIColor(red: 223/225, green: 178/255, blue: 35/255, alpha: 1)
        backgroundView.layer.cornerRadius = 15
        cell.selectedBackgroundView = backgroundView

        cell.separatorInset = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
        return cell
    }
  
    @IBAction func confirmButton(_ sender: UIButton) {
        let storyboard =  UIStoryboard(name: "Login", bundle: nil)
        let userLogin = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(userLogin, animated: true)
        
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
