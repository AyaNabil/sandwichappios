//
//  ThirdPageViewController.swift
//  SandwishApp
//
//  Created by mac on 3/3/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class ThirdPageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func nextButtonAction(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Start", bundle:nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LanguageViewController") as! LanguageViewController
        self.navigationController?.pushViewController(vc, animated: true)

        
        
    }
    
    
    @IBAction func skipButtonAction(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Start", bundle:nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SecondPageViewController") as! SecondPageViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }

}
