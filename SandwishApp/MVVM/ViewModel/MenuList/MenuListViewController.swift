//
//  MenuListViewController.swift
//  SandwishApp
//
//  Created by mac on 3/9/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class MenuListViewController: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    var imageNames : [String] = []
    var labelNames : [String] = []
    var labelNameslide : [String] = []
  
    
     @IBOutlet var menulistCollectionview: UICollectionView!
     @IBOutlet var menulistCollectionviewOne: UICollectionView!
     @IBOutlet var menulistCollectionviewTwo: UICollectionView!
     @IBOutlet var slideCollectionview: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
    menulistCollectionview.delegate = self
    menulistCollectionview.dataSource = self
        //one
        menulistCollectionviewOne.delegate = self
        menulistCollectionviewOne.dataSource = self
        //two
        
        menulistCollectionviewTwo.delegate = self
        menulistCollectionviewTwo.dataSource = self
        
        //slidemenu
        slideCollectionview.delegate = self
        slideCollectionview.dataSource = self
        slideCollectionview.allowsMultipleSelection = false
        
        
        
        labelNames = ["Fried Chicken Box" , "Fried Chicken Box" , "Fried Chicken Box" , "Fried Chicken Box"   , "Fried Chicken Box" ]
       
        imageNames = ["whiteKfc" , "redKfc" , "whiteKfc" , "redKfc" , "whiteKfc"]
        
        labelNameslide = ["Chickens" , "Sandwiches" , "Salades" , "Chickens" , "Sandwiches" , "Salades" ]
       
        
     
    }
    
    //collectionview
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == menulistCollectionview
        {
        return labelNames.count
        }
        else if collectionView == menulistCollectionviewOne
        {
            return labelNames.count
        }
        
        else if collectionView == menulistCollectionviewTwo{
             return labelNames.count
        }
        
        else{
            return labelNameslide.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == menulistCollectionview {
        let cell = menulistCollectionview.dequeueReusableCell(withReuseIdentifier: "MenulistCollectionviewCell", for: indexPath)
            as! MenulistCollectionviewCell
        cell.menutitle.text = labelNames[indexPath.row]
        cell.menuPicture.image = UIImage(named:imageNames[indexPath.row])
        //shadow for vew
        cell.view.layer.masksToBounds = false
        cell.view.layer.shadowRadius = 3.0
        cell.view.layer.shadowColor = UIColor.gray.cgColor
        cell.view.layer.shadowOffset =  CGSize(width: 1, height: 4)
        cell.view.layer.shadowOpacity = 0.6
//        //cell color
//
//        let backgroundView = UIView()
//        backgroundView.backgroundColor = UIColor.orange
//        backgroundView.backgroundColor = UIColor(red: 212/225, green: 156/255, blue: 241/255, alpha: 1)
//        //        backgroundView.layer.cornerRadius = 25
     //   cell.selectedBackgroundView = backgroundView
        return cell
        }
        
        else if collectionView == menulistCollectionviewOne{
            let cellOne = menulistCollectionviewOne.dequeueReusableCell(withReuseIdentifier: "MenulistCollectionviewOneCell", for: indexPath)
                as! MenulistCollectionviewOneCell
            cellOne.menutitle.text = labelNames[indexPath.row]
            cellOne.menuPicture.image = UIImage(named:imageNames[indexPath.row])
            //shadow for vew
            cellOne.view.layer.masksToBounds = false
            cellOne.view.layer.shadowRadius = 3.0
            cellOne.view.layer.shadowColor = UIColor.gray.cgColor
            cellOne.view.layer.shadowOffset =  CGSize(width: 1, height: 4)
            cellOne.view.layer.shadowOpacity = 0.6
            return cellOne
            
        }
        
        else if collectionView == menulistCollectionviewTwo{
            
            let cellTwo = menulistCollectionviewTwo.dequeueReusableCell(withReuseIdentifier: "MenulistCollectionviewTwoCell", for: indexPath)
                as! MenulistCollectionviewTwoCell
            cellTwo.menutitle.text = labelNames[indexPath.row]
            cellTwo.menuPicture.image = UIImage(named:imageNames[indexPath.row])
            //shadow for vew
            cellTwo.view.layer.masksToBounds = false
            cellTwo.view.layer.shadowRadius = 3.0
            cellTwo.view.layer.shadowColor = UIColor.gray.cgColor
            cellTwo.view.layer.shadowOffset =  CGSize(width: 1, height: 4)
            cellTwo.view.layer.shadowOpacity = 0.6
            return cellTwo
        }
        
        
        else{
            let cellslide = slideCollectionview.dequeueReusableCell(withReuseIdentifier: "SlideMenuCollectionViewCell", for: indexPath)
                as! SlideMenuCollectionViewCell
            cellslide.menutitle.text = labelNameslide[indexPath.row]

            return cellslide
        }

    }

  

    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if collectionView ==  menulistCollectionviewOne{
 
        return UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 20)
    }
        
        else if collectionView == menulistCollectionviewTwo {
            return UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 20)
        }
        
        
        else {
            
             return UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 20)
        }
}
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == slideCollectionview {
            return 0
        }
        else{
            
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == slideCollectionview {
//            let storyboard =  UIStoryboard(name: "Order", bundle: nil)
//            let userLogin = storyboard.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
//            self.navigationController?.pushViewController(userLogin, animated: true)
            print("hi")
        }
        
        else{
            let storyboard =  UIStoryboard(name: "Order", bundle: nil)
            let userLogin = storyboard.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
            self.navigationController?.pushViewController(userLogin, animated: true)
        }
    }
    

    
    @IBAction func reviewbutton(_ sender: UIButton) {
        
        let storyboard =  UIStoryboard(name: "Review", bundle: nil)
        let userLogin = storyboard.instantiateViewController(withIdentifier: "ReviewsViewController") as! ReviewsViewController
        self.navigationController?.pushViewController(userLogin, animated: true)
        
        
    }
    
    
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
