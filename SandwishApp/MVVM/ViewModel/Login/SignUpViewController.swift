//
//  SignUpViewController.swift
//  SandwishApp
//
//  Created by mac on 3/4/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var confirmpasswordTF: UITextField!
    @IBOutlet weak var firstnameTextField: UITextField!
    @IBOutlet weak var lastnameTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        //password
        passwordTF.isSecureTextEntry = true
        passwordTF.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: passwordTF.frame.height))
        passwordTF.leftViewMode = .always
        
        //confirmpassword
        confirmpasswordTF.isSecureTextEntry = true
        confirmpasswordTF.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: confirmpasswordTF.frame.height))
        confirmpasswordTF.leftViewMode = .always

        //email
        emailTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: emailTextField.frame.height))
        emailTextField.leftViewMode = .always
        
        
        //firstname
        firstnameTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: firstnameTextField.frame.height))
        firstnameTextField.leftViewMode = .always
        
        //lastname
        lastnameTF.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: lastnameTF.frame.height))
        lastnameTF.leftViewMode = .always
        
        
        //phone
        phoneTF.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: phoneTF.frame.height))
        phoneTF.leftViewMode = .always
        
        
        //shadow for email
        emailTextField.layer.masksToBounds = false
        emailTextField.layer.shadowRadius = 3.0
        emailTextField.layer.shadowColor = UIColor.gray.cgColor
        emailTextField.layer.shadowOffset =  CGSize(width: 1, height: 4)
        emailTextField.layer.shadowOpacity = 0.6
        
        
        //shadow for password
        passwordTF.layer.masksToBounds = false
        passwordTF.layer.shadowRadius = 3.0
        passwordTF.layer.shadowColor = UIColor.gray.cgColor
        passwordTF.layer.shadowOffset =  CGSize(width: 1, height: 4)
        passwordTF.layer.shadowOpacity = 0.6
        
        
        
        //shadow for confirmpassword
        confirmpasswordTF.layer.masksToBounds = false
        confirmpasswordTF.layer.shadowRadius = 3.0
        confirmpasswordTF.layer.shadowColor = UIColor.gray.cgColor
        confirmpasswordTF.layer.shadowOffset =  CGSize(width: 1, height: 4)
        confirmpasswordTF.layer.shadowOpacity = 0.6
        
        
        //shadow for firstname
        firstnameTextField.layer.masksToBounds = false
        firstnameTextField.layer.shadowRadius = 3.0
        firstnameTextField.layer.shadowColor = UIColor.gray.cgColor
        firstnameTextField.layer.shadowOffset =  CGSize(width: 1, height: 4)
        firstnameTextField.layer.shadowOpacity = 0.6
        
        
        //shadow for lastname
        lastnameTF.layer.masksToBounds = false
        lastnameTF.layer.shadowRadius = 3.0
        lastnameTF.layer.shadowColor = UIColor.gray.cgColor
        lastnameTF.layer.shadowOffset =  CGSize(width: 1, height: 4)
        lastnameTF.layer.shadowOpacity = 0.6
        
        
        //shadow for phone
        phoneTF.layer.masksToBounds = false
        phoneTF.layer.shadowRadius = 3.0
        phoneTF.layer.shadowColor = UIColor.gray.cgColor
        phoneTF.layer.shadowOffset =  CGSize(width: 1, height: 4)
        phoneTF.layer.shadowOpacity = 0.6
        
    }
    
    
    @IBAction func confirmpasswordClicked(_ sender: Any) {
        
        (sender as! UIButton).isSelected = !(sender as! UIButton).isSelected
        if (sender as! UIButton).isSelected {
            confirmpasswordTF.isSecureTextEntry = true
        } else {
            confirmpasswordTF.isSecureTextEntry = false
        }
    }
    
    
    @IBAction func passwordClicked(_ sender: Any) {
        
        (sender as! UIButton).isSelected = !(sender as! UIButton).isSelected
        if (sender as! UIButton).isSelected {
            passwordTF.isSecureTextEntry = true
        } else {
            passwordTF.isSecureTextEntry = false
        }
    }

    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func signUp(_ sender: UIButton) {
        
        
        
    }
    
    @IBAction func signIn(_ sender: UIButton) {
        
        let storyboard =  UIStoryboard(name: "Login", bundle: nil)
        let userLogin = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(userLogin, animated: true)
    }
    
}
