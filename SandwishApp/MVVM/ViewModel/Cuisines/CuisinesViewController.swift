//
//  CuisinesViewController.swift
//  SandwishApp
//
//  Created by mac on 3/9/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class CuisinesViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    var labelNames : [String] = []
    @IBOutlet var cuisinesCollectionview: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cuisinesCollectionview.delegate = self
        cuisinesCollectionview.dataSource = self
        cuisinesCollectionview.allowsMultipleSelection = true
     labelNames = ["Africa" , "Asian" , "BreakFast" , "Burger" , "Chinese" , "Coffee" , "Fast Food" , "Sandwiche" , "Shawerma" , "Africa" , "Asian" , "BreakFast" , "Burger" , "Chinese" , "Coffee" , "Fast Food" , "Sandwiche" , "Shawerma", "Asian" , "BreakFast" , "Burger" , "Chinese" , "Coffee" , "Fast Food" , "Sandwiche" , "Shawerma" , "Africa" , "Asian" , "BreakFast" , "Burger" , "Chinese" , "Coffee" , "Fast Food" ]
    }
    
    
    //collectionview
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return labelNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = cuisinesCollectionview.dequeueReusableCell(withReuseIdentifier: "CuisinesCollectionViewCell", for: indexPath)
            as! CuisinesCollectionViewCell
        cell.cuisinestitle.text = labelNames[indexPath.row]
        //shadow for vew
        cell.layer.masksToBounds = false
        cell.layer.shadowRadius = 3.0
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset =  CGSize(width: 1, height: 4)
        cell.layer.shadowOpacity = 0.3
        return cell
    }

    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 110, height: 55)
    }
 
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
       
            return UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
        
    }

    
    @IBAction func confirmButton(_ sender: UIButton) {
//        let storyboard =  UIStoryboard(name: "Review", bundle: nil)
//        let userLogin = storyboard.instantiateViewController(withIdentifier: "WriteReviewViewController") as! WriteReviewViewController
//        self.navigationController?.pushViewController(userLogin, animated: true)
        
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
