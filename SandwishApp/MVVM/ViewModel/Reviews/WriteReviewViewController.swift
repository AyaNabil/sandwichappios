//
//  WriteReviewViewController.swift
//  SandwishApp
//
//  Created by mac on 3/8/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class WriteReviewViewController: UIViewController {
  @IBOutlet var reviewtextview: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        //shadow for textfield
        reviewtextview.layer.masksToBounds = false
        reviewtextview.layer.shadowRadius = 3.0
        reviewtextview.layer.shadowColor = UIColor.gray.cgColor
        reviewtextview.layer.shadowOffset =  CGSize(width: 1, height: 4)
        reviewtextview.layer.shadowOpacity = 0.6
    }
    
    @IBAction func backButton(_ sender: UIButton) {
                self.navigationController?.popViewController(animated: true)
        
    }
    
       @IBAction func confirmButton(_ sender: UIButton) {
//        let storyboard =  UIStoryboard(name: "Home", bundle: nil)
//        let userLogin = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//        self.navigationController?.pushViewController(userLogin, animated: true)
        
    }


}
