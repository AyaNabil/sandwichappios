//
//  OrderOnwayViewController.swift
//  SandwishApp
//
//  Created by mac on 3/5/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class OrderOnwayViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func backButton(_ sender: UIButton) {
//        self.navigationController?.popViewController(animated: true)
        let storyboard =  UIStoryboard(name: "Home", bundle: nil)
        let userLogin = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(userLogin, animated: true)
    }

}
