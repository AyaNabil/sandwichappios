//
//  SlideMenuCollectionViewCell.swift
//  SandwishApp
//
//  Created by mac on 3/9/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class SlideMenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var menutitle: UILabel!
    
    override var isSelected: Bool {
        didSet {
            if self.isSelected {
                backgroundColor = UIColor(red: 212/225, green: 156/255, blue: 241/255, alpha: 1)
            }
            else {
            backgroundColor = UIColor(displayP3Red: 240/255, green: 198/255, blue: 61/255, alpha: 1)
            }
        }
    }
}
