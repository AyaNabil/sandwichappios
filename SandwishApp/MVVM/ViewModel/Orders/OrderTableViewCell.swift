//
//  OrderTableViewCell.swift
//  SandwishApp
//
//  Created by mac on 3/8/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {
    @IBOutlet weak var ordertitle: UILabel!
    @IBOutlet weak var orderPicture: UIImageView!
    @IBOutlet weak var view: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        //shadow for vew
        view.layer.masksToBounds = false
        view.layer.shadowRadius = 3.0
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset =  CGSize(width: 1, height: 4)
        view.layer.shadowOpacity = 0.6
    }

}
