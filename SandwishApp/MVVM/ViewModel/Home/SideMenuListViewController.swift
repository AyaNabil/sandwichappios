//
//  SideMenuListViewController.swift
//  SandwishApp
//
//  Created by mac on 3/5/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class SideMenuListViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
//    let gradient: CAGradientLayer = CAGradientLayer()
    
    @IBOutlet var sideMenuTableView: UITableView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var usernamelabel: UILabel!

    var imageNames : [String] = []
    var labelNames : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        sideMenuTableView.separatorInset = .zero
        sideMenuTableView.layoutMargins = .zero
        sideMenuTableView.delegate = self
        sideMenuTableView.dataSource = self
        
        sideMenuTableView.isUserInteractionEnabled = true
        
        labelNames = ["Home" , "Orders ","LogOut"]
        
        imageNames = ["Home" , "Cart" , "openpass"]
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = sideMenuTableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell", for: indexPath) as!
        SideMenuTableViewCell
        cell.sideMenutitle.text = labelNames[indexPath.row]
        cell.sideMenuPicture.image = UIImage(named:imageNames[indexPath.row])
        let backgroundView = UIView()
        //        backgroundView.backgroundColor = UIColor.orange
        backgroundView.backgroundColor = UIColor(red: 223/225, green: 178/255, blue: 35/255, alpha: 1)
        backgroundView.layer.cornerRadius = 25
        cell.selectedBackgroundView = backgroundView
        
        cell.separatorInset = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
        return cell
    }
    
    
    
    //didselect
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {


        sideMenuTableView.deselectRow(at: indexPath, animated: true)
        let cell = sideMenuTableView.cellForRow(at: indexPath) as! SideMenuTableViewCell

        switch indexPath.row  {
        case 0:

//            cell.managerSideMenuPicture.image = UIImage.init(named: "manuserorange")
//
//            cell.managerSideMenutitle.textColor = UIColor(red: 249/225, green: 99/255, blue: 50/255, alpha: 1)

            let sb = UIStoryboard(name: "Home", bundle:nil)
            let vc = sb.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(vc, animated: true)


        //orders
        case 1:

     

            let sb = UIStoryboard(name: "Order", bundle:nil)
            let vc = sb.instantiateViewController(withIdentifier: "OrdersViewController") as! OrdersViewController

            self.navigationController?.pushViewController(vc, animated: true)


        case 2:


            let sb = UIStoryboard(name: "Login", bundle:nil)
            let vc = sb.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController

            self.navigationController?.pushViewController(vc, animated: true)

      
            break
        default:


            break
        }
    }
    
    
    //diddeselect
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        managerSideMenuTableView.deselectRow(at: indexPath, animated: true)
//        let cell = managerSideMenuTableView.cellForRow(at: indexPath) as! SideMenuTableViewCell
//
//        switch indexPath.row  {
//        case 0:
//            cell.managerSideMenuPicture.image = UIImage.init(named: "manusermenu")
//
//            cell.managerSideMenutitle.textColor = UIColor(red: 47/225, green: 59/255, blue: 83/255, alpha: 1)
//
//
//        case 1:
//            cell.managerSideMenuPicture.image = UIImage.init(named:"tagsmenu")
//            cell.managerSideMenutitle.textColor = UIColor(red: 47/225, green: 59/255, blue: 83/255, alpha: 1)
//        case 2:
//            cell.managerSideMenuPicture.image = UIImage.init(named: "progressreportmenu")
//            cell.managerSideMenutitle.textColor = UIColor(red: 47/225, green: 59/255, blue: 83/255, alpha: 1)
//
//        case 3:
//            cell.managerSideMenuPicture.image = UIImage.init(named: "close-envelopemenu")
//            cell.managerSideMenutitle.textColor = UIColor(red: 47/225, green: 59/255, blue: 83/255, alpha: 1)
//
//        case 4:
//            cell.managerSideMenuPicture.image = UIImage.init(named:"multipleusersmenu")
//            cell.managerSideMenutitle.textColor = UIColor(red: 47/225, green: 59/255, blue: 83/255, alpha: 1)
//
//        case 5:
//            cell.managerSideMenuPicture.image = UIImage.init(named:"listmenu")
//            cell.managerSideMenutitle.textColor = UIColor(red: 47/225, green: 59/255, blue: 83/255, alpha: 1)
//
//        case 6:
//            cell.managerSideMenuPicture.image = UIImage.init(named:"newusermenu")
//            cell.managerSideMenutitle.textColor = UIColor(red: 47/225, green: 59/255, blue: 83/255, alpha: 1)
//        case 7:
//            cell.managerSideMenuPicture.image = UIImage.init(named:"analyticsmenu")
//            cell.managerSideMenutitle.textColor = UIColor(red: 47/225, green: 59/255, blue: 83/255, alpha: 1)
//        case 8:
//            cell.managerSideMenuPicture.image = UIImage.init(named:"logoutmenu")
//            cell.managerSideMenutitle.textColor = UIColor(red: 47/225, green: 59/255, blue: 83/255, alpha: 1)
//        default:
//
//
//            break
//        }
//    }
    
    
    @IBAction func accountbutton(_ sender: UIButton) {
        
        let storyboard =  UIStoryboard(name: "Home", bundle: nil)
        let userLogin = storyboard.instantiateViewController(withIdentifier: "AccountSettingViewController") as! AccountSettingViewController
        self.navigationController?.pushViewController(userLogin, animated: true)
        //        //           self.present(userLogin, animated: true)
        
    }
}

