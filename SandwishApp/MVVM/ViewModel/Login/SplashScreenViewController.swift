//
//  SplashScreenViewController.swift
//  SandwishApp
//
//  Created by mac on 3/3/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController  , UIViewControllerTransitioningDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.perform(#selector(self.presentWithCustomAnimation), with: nil, afterDelay: 3)
    }
    
    //animation function
    @objc func presentWithCustomAnimation (){
        
        
        let storyboard =  UIStoryboard(name: "Start", bundle: nil)
        let userlogin = storyboard.instantiateViewController(withIdentifier: "FirstPageViewController") as! FirstPageViewController
        navigationController?.pushViewController(userlogin, animated: true)
        
    }
}
