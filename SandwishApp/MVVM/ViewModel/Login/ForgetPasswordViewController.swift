//
//  ForgetPasswordViewController.swift
//  SandwishApp
//
//  Created by mac on 3/4/20.
//  Copyright © 2020 Wasma. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: UIViewController {
    @IBOutlet weak var confirmEmailTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

    
        //email
        emailTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: emailTextField.frame.height))
        emailTextField.leftViewMode = .always
        
        //shadow for email
        emailTextField.layer.masksToBounds = false
        emailTextField.layer.shadowRadius = 3.0
        emailTextField.layer.shadowColor = UIColor.gray.cgColor
        emailTextField.layer.shadowOffset =  CGSize(width: 1, height: 4)
        emailTextField.layer.shadowOpacity = 0.6
      
        //confiem email
        confirmEmailTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: confirmEmailTextField.frame.height))
        confirmEmailTextField.leftViewMode = .always
        
        //shadow for confiem email
        confirmEmailTextField.layer.masksToBounds = false
        confirmEmailTextField.layer.shadowRadius = 3.0
        confirmEmailTextField.layer.shadowColor = UIColor.gray.cgColor
        confirmEmailTextField.layer.shadowOffset =  CGSize(width: 1, height: 4)
        confirmEmailTextField.layer.shadowOpacity = 0.6
    }
    
    @IBAction func sendLinkButton(_ sender: UIButton) {
        
        //        let storyboard =  UIStoryboard(name: "User", bundle: nil)
        //        let forgetpass = storyboard.instantiateViewController(withIdentifier: "ForgetPasswordVC") as! ForgetPasswordVC
        //        self.navigationController?.pushViewController(forgetpass, animated: true)
        //        //        self.present(forgetpass, animated: true)
        
    }
    
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }

}
